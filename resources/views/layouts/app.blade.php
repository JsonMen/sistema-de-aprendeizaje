<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .profile-card-5{
            margin-top:20px;
        }
        .profile-card-5 .btn{
            border-radius:2px;
            text-transform:uppercase;
            font-size:12px;
            padding:7px 20px;
        }
        .profile-card-5 .card-img-block {
            width: 91%;
            margin: 0 auto;
            position: relative;
            top: -20px;

        }
        .profile-card-5 .card-img-block img{
            height: 300px;
            border-radius:5px;
            box-shadow:0 0 10px rgba(0,0,0,0.63);
        }
        .profile-card-5 h5{
            color:#4E5E30;
            font-weight:600;
        }
        .profile-card-5 p{
            font-size:14px;
            font-weight:300;
        }
        .profile-card-5 .btn-primary{
            background-color:#4E5E30;
            border-color:#4E5E30;
        }


    </style>
</head>
<body>
    <div id="app">
        @auth
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">

                    <a class="navbar-brand" href="{{ route('front.index') }}">
                    {{ 'Desarrollo'}}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('users.index')}}">Usuarios</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('categories.index')}}">Categorias</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('articles.index')}}">Articulos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('images.index')}}">Imagenes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('tags.index')}}">Tags</a>
                            </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                           {{-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>--}}
                          {{--  <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>--}}
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @endauth
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
