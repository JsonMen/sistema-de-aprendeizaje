<div class="text-center">
	<br>
	<img src="{{ asset('/uploads/logo.svg') }}"  class="logo" style="width: 150px; height: 150px;">

	<br>
	
	<h1 class="title-front-header">SISTEMA DE CURSOS</h1>
    @guest
        <h2><a href="{{ route('login') }}">Loguearse</a></h2>

    @else
        @if(auth()->user()->admin())
            <h2><a href="{{route('users.index')}}">Eres administrador</a></h2>
        @else
            <div aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Cerrar') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        @endif
    @endguest
</div>
