@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Articulos</div>
            <div class="card-body">
                {!! Form::model($article,['route'=> ['articles.update',$article->id],'method'=>'PUT','files'=>true]) !!}
                @include('admin.articles.form')
                <div class="form-group">
                    {!! Form::submit('Actualizar',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
