@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Articulos</div>
            <div class="card-body">
                {!! Form::open([ 'route'=>'articles.store','method'=>'POST','files'=>true]) !!}
                    @include('admin.articles.form')
                    <div class="form-group">
                        {!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
