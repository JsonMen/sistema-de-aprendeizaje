<div class="form-group{{ $errors->has('title')? ' has-error' : '' }}">
    {!! Form::label('title','Titulo') !!}
    {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Titulo del articulo']) !!}
    @if($errors->has('title'))
        <small class="form-text text-muted">
            <strong>{{$errors->first('title')}}</strong>
        </small>
    @endif
</div>
<div class="">
    {!! Form::label('category_id','Categoria') !!}
    {!! Form::select( 'category_id',$cate,null,['class'=>'form-control']) !!}
</div>
<div class="form-group{{ $errors->has('title')? ' has-error' : '' }}">
    {!! Form::label('content','Contenido') !!}
    {!! Form::textarea('content',null,['class'=>'form-control']) !!}
    @if($errors->has('content'))
        <small class="form-text text-muted">
            <strong>{{$errors->first('content')}}</strong>
        </small>
    @endif
</div>
<div class="form-group">
    {!! Form::label('tags_id','Tags') !!}
    {!! Form::select( 'tags_id',$tags,null,['class'=>'form-control']) !!}
</div>
<div class="form-group{{ $errors->has('title')? ' has-error' : '' }}">
    {!! Form::label('image','Imagen') !!}
    {!! Form::file('image') !!}
    @if($errors->has('image'))
        <small class="form-text text-muted">
            <strong>{{$errors->first('image')}}</strong>
        </small>
    @endif
</div>
