@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="{{route('articles.create')}}" class="btn btn-info">Crear Articulos</a>
        <table class="table">
            <thead>
                <th>ID</th>
                <th>Titulo</th>
                <th>Categoria</th>
                <th>User</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                @foreach($article as $a)
                    <tr>
                        <th>{{ $a->id }}</th>
                        <th>{{ $a->title }}</th>
                        <th>{{ $a->category->name }}</th>
                        <th>{{ $a->user->name }}</th>
                        <td>
                            <a href="{{ route('articles.edit',$a->id) }}" class="btn btn-warning"><i class="fas fa-pen-square"></i></a>
                            <a href="{{ route('articles.destroy',$a->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
