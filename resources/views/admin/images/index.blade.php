@extends('layouts.app')
@section('content')

    <section>
        <div class="container">
            <div class="row">
                <!--Profile Card 5-->
                @foreach($image as $image)
                <div class="col-md-4 mt-4">
                    <div class="card profile-card-5">
                        <div class="card-img-block">
                            <img class="card-img-top" src="/uploads/articles/{{$image->name}}" alt="Card image cap">
                        </div>
                        <div class="card-body pt-0">
                            <h5 class="card-title">{{ $image->article->title }}</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
