@extends('layouts.app')
@section('content')
    <div class="container">
        <nav class="navbar navbar-light bg-light justify-content-between">
            <a href="{{route('tags.create')}}" class="btn btn-info">Crear Tags</a>
            {!! Form::open(['route'=>'tags.index','method'=>'GET','class'=>'form-inline']) !!}
                {!! Form::text('name',null,['class'=>'form-control mr-sm-2','type'=>'search','placeholder'=>'Search','aria-label'=>'Search']) !!}
            {!! Form::close() !!}
        </nav>
        @if(!empty($tag))
            <table class="table">

                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Accione</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tag as $tag)
                    <tr>
                        <td>{{$tag->id}}</td>
                        <td>{{$tag->name}}</td>
                        <td>
                            <a href="{{ route('tags.edit',$tag->id) }}" class="btn btn-warning"><i class="fas fa-pen-square"></i></a>
                            <a href="{{ route('tags.destroy',$tag->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-warning" role="alert">
                No hay Tags
            </div>
        @endif
    </div>
@endsection
