@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Categories</div>
            <div class="card-body">
                {!!  Form::open(['route'=>'categories.store','method'=>'POST']) !!}
                    <div class="form-group{{ $errors->has('name')? 'has-error' : '' }}">
                        {!! Form::label( 'name', 'Nombre') !!}
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre de la categoria']) !!}
                        @if($errors->has('name'))
                            <small class="form-text text-muted">
                                <strong>{{$errors->first('name')}}</strong>
                            </small>

                        @endif
                    </div>
                <div class="form-group">
                    {!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

