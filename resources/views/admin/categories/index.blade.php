@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="{{route('categories.create')}}" class="btn btn-info">Crear Categorias</a>
        @if(!empty($cate))
            <table class="table">

                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Accione</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cate as $cate)
                    <tr>
                        <td>{{$cate->id}}</td>
                        <td>{{$cate->name}}</td>
                        <td>
                            <a href="{{ route('categories.edit',$cate->id) }}" class="btn btn-warning"><i class="fas fa-pen-square"></i></a>
                            <a href="{{ route('categories.destroy',$cate->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-warning" role="alert">
                No hay Categorias
            </div>
        @endif
    </div>
@endsection
