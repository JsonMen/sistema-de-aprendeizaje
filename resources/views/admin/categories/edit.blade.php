@extends('layouts.app')
@section('content')
    <div class="container">
        {!!  Form::model($cate,['route'=>['categories.update',$cate->id],'method'=>'PUT']) !!}
        <div class="form-group{{ $errors->has('name')? 'has-error' : '' }}">
            {!! Form::label( 'name', 'Nombre') !!}
            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre de la categoria']) !!}
            @if($errors->has('name'))
                <small class="form-text text-muted">
                    <strong>{{$errors->first('name')}}</strong>
                </small>

            @endif
        </div>
        <div class="form-group">
            {!! Form::submit('Actualizar',['class'=>'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection
