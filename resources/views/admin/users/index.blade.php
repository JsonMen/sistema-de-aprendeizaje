@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{route('users.create')}}" class="btn btn-info">Crear Usuario</a>
        <div class="card">

            @if(!empty($user))
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Email</th>
                    <th scope="col">Type</th>
                    <th scope="col">Accione</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($user as $users)
                        <tr>
                            <td>{{$users->id}}</td>
                            <td>{{$users->name}}</td>
                            <td>{{$users->email}}</td>
                            <td>
                                @if($users->type=="admin")
                                    <span class="badge badge-danger">{{$users->type}}</span>
                                @else
                                    <span class="badge badge-primary">{{$users->type}}</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('users.edit',$users->id)}}" class="btn btn-warning"><i class="fas fa-pen-square"></i></a>
                                <a href="{{route('users.destroy',$users->id)}}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a></td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                <div class="alert alert-warning" role="alert">
                    No hay usuarios para mostrar
                </div>
            @endif
            {!! $user->render() !!}
        </div>
    </div>

@endsection

