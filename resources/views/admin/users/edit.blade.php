@extends('layouts.app')
@section('content')
    <div class="container">
        {!! Form::model($user,['route'=>['users.update',$user->id],'method'=>'PUT']) !!}

        @include('admin.users.form.form')
        <div class="form-group">
            {!! Form::submit( 'Actualizar',['class'=>'btn btn-primary'])  !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection
