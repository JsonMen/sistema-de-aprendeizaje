<div class="form-group{{ $errors->has('name')? ' has-error' : '' }}">
{!! Form::label('name','Nombre') !!}
{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre Completo','required']) !!}
    @if($errors->has('name'))
        <span class="alert alert-danger">
            <strong>{{$errors->first('name')}}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('email')? ' has-error' : '' }}">
    {!! Form::label('email','Correo Electronico') !!}
    {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'example@gmail.com','required']) !!}
    @if($errors->has('email'))
        <span class="alert alert-danger">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
</div>
@isset($type)
    @if($type=="crear")
        <div class="form-group{{$errors->has('password') ? ' has-error' : ''}}">
            {!! Form::label('password','Password') !!}
            {!! Form::password('password',['class'=>'form-control','placeholder'=>'******','required']) !!}
            @if($errors->has('password'))
                <span class="alert alert-danger" role="alert">
                    <strong>{{$errors->first('password')}}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            {!! Form::label('type','Tipo de usuario') !!}
            {!! Form::select('type',['member'=>'Miembro','admin'=>'Administrador'],null,['class'=>'form-control']) !!}
        </div>

    @endif
@endisset
