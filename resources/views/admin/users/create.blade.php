@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Usuarios</div>
            <div class="card-body">
                {!! Form::open(['route'=>'users.store','method'=>'POST']) !!}
                    @include('admin.users.form.form')
                    <div class="form-group">
                        {!! Form::submit( 'Registrar',['class'=>'btn btn-primary'])  !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

