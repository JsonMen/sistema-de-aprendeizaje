<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:5',
            'email'=>'required|unique:users',
            'password'=>'required|min:5'
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'El nombre es requerido',
            'name.min'=>'El nombre debe ser mayor a 5 caracteres',
            'email.unique'=>'El correo ya esta en uso',
            'email.required'=>'EL corre es requerido',
            'password.required'=>'El password es requerido',
            'password.min'=>'El password minimo 5 caracteres'
        ];
    }
}
