<?php
namespace App\Http\ViewComposers;
use App\Category;
use App\Tag;
use Illuminate\View\View;
use App\Repositories\UserRepository;
class AsideComposer
{
    public function compose(View $view)
    {
        $cate=Category::all();
        $tags=Tag::all();
        $view->with('categories',$cate)->with('tags',$tags);
    }
}
