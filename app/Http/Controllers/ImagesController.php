<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;

class ImagesController extends Controller
{
    public function index()
    {
        $image=Image::all();
        $image->each(function($image){
            $image->article;
        });
        return view('admin.images.index',compact('image'));
    }
}
