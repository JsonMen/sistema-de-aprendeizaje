<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;

class CategoriesController extends Controller
{
    public function index(){
        $cate=Category::all();
        return view('admin.categories.index',compact('cate'));
    }
    public function destroy($id)
    {
        $cate=Category::find($id);
        $cate->delete();
        return redirect()->route('categories.index');
    }
    public function create()
    {
        return view('admin.categories.create');
    }
    public function store(CategoryRequest $request){
        $cate=new Category($request->all());
        $cate->save();
        return redirect()->route('categories.index');
        //dd($request->all());
    }
    public function edit($id)
    {
        $cate=Category::find($id);
        return view('admin.categories.edit',compact('cate'));
    }
    public function update(CategoryRequest $request,$id)
    {
        $cate=Category::find($id);
        $cate->fill($request->all());
        $cate->save();
        return redirect()->route('categories.edit',$id);
    }

}
