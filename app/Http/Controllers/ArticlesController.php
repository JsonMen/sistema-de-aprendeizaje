<?php

namespace App\Http\Controllers;

use App\Article;
use App\Image;
use App\Tag;
use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests\ArticleRequest;
class ArticlesController extends Controller
{
    public function index(){
        $article=Article::all();
        $article->each(function($article){
            $article->category;
            $article->user;
        });
        //dd($article);
        return view('admin.articles.index',compact('article'));
    }
    public function create()
    {
        $cate=Category::orderBy('name','DESC')->pluck('name','id');
        $tags=Tag::orderBy('name','DESC')->pluck('name','id');
        return view('admin.articles.create',compact('cate','tags'));
    }
    public function store(ArticleRequest $request)
    {
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $name =time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/articles');
            //$imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);

        }

        $article=new Article($request->all());
        $article->user_id=auth()->user()->id;
        $article->save();

        $article->tags()->sync($request->tags_id);


        $images=new Image();
        $images->name=$name;
        // esta parte recibe del modelo de las relaciones y asocia el modelo
        $images->article()->associate($article);
        $images->save();

        return redirect()->route('articles.index');
    }
    public function edit($id)
    {
        $article=Article::find($id);
        $cate=Category::all()->pluck('name','id');
        $tags=Tag::all()->pluck('name','id');
        return view('admin.articles.edit',compact('cate','article','tags'));
    }
    public function update(Request $request,$id)
    {

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $name =time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/articles');
            //$imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $imag=Image::where('article_id','=',$id)->firstOrFail();
            $file_path=public_path().'/uploads/articles/'.$imag->name;
            $imag->name=$name;
            $imag->save();
            \File::delete($file_path);

        }
        // actualizar
        $article=Article::find($id);
        $article->fill($request->all());
        // actualizar article_tag
        $article->tags()->sync($request->input('tags_id'));
        $article->save();
        return redirect()->route('articles.index');
    }
    public function destroy($id)
    {
        // primero eliminar
        /*
            1.- la imagen de la base de datos
            2.- borrar la imagen de la carpeta

            3.- borrar de article_tag
            4.- borrar recien article
        */
        $imageroute=Image::where('article_id','=',$id)->firstOrFail();
        $file=public_path().'/uploads/articles/'.$imageroute->name;
        \File::delete($file);

        $article=Article::find($id);
        $article->delete();
        return redirect()->route('articles.index');
    }
}
