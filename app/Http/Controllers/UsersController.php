<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        $user=User::orderBy('id','DESC')->paginate(15);
        return view('admin.users.index',compact('user'));
    }
    public function create()
    {
        $type="crear";
        return view('admin.users.create',compact('type'));
    }
    public function store(UserRequest $request)
    {
        $user=new User($request->all());
        $user->password=bcrypt($request->password);
        $user->save();
        return redirect()->route('users.index');
    }
    public function edit($id)
    {
        $user=User::find($id);

        return view('admin.users.edit',compact('user'));
    }
    public function update(Request $request,$id)
    {
        $user=User::find($id);
        //$user->name=$request->name;
        //$user->email=$request->email;
        $user->fill($request->all());
        $user->save();
        return redirect()->route('users.index');
    }
    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();
        return redirect()->route('admin.users.index');
    }
}
