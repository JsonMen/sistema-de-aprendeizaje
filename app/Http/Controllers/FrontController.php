<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Tag;

class FrontController extends Controller
{
    public function index()
    {
        $articles = Article::all();
        $articles->each(function($articles){
            $articles->category;
            $articles->images;

        });

        return view('front.index',compact('articles'));
    }



    public function searchCategory($name)
    {

        $category = Category::where('name','=',$name)->first();
        $articles=$category->articles;

        $articles->each(function($articles){
            $articles->category;
            $articles->images;

        });

        return view('front.index')->with('articles', $articles);

    }
    public function searchTag($name)
    {

        $tag = Tag::where('name','=',$name)->first();
        $articles = $tag->articles;

        $articles->each(function($articles){
            $articles->category;
            $articles->images;

        });

        return view('front.index')->with('articles', $articles);

    }
    public function viewArticle($slug)
    {

        $article = Article::where('slug','=',$slug)->first();

        $article->category;
        $article->user;
        $article->tags;
        $article->images;

        return view('front.article')->with('article', $article);

    }
}
