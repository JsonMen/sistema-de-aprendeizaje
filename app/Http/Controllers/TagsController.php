<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;

class TagsController extends Controller
{
    public function index(Request $request){
        //$name=$request->input('name');

        //$tag=Tag::name($name)->orderByCreated()->get();
        $tag=Tag::all();
        return view('admin.tags.index',compact('tag'));
    }
    public function create()
    {
        return view('admin.tags.create');
    }
    public function store(TagRequest $request)
    {
        $tag=new Tag($request->all());
        $tag->save();
        return redirect()->route('tags.index');
    }
    public function destroy($id)
    {

        $tag=Tag::find($id);
        $tag->delete();
        return redirect()->route('tags.index');
    }
    public function edit($id)
    {
        $tag=Tag::find($id);
        return view('admin.tags.edit',compact('tag'));
    }
    public function update(TagRequest $request,$id)
    {
        dd($id);
    }
}
