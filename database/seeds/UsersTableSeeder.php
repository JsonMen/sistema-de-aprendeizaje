<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class,400)->create();
        App\user::create([
           'name'=>'yamil',
           'email'=>'yamil@gmail.com',
           'password'=>bcrypt('123456789'),
            'type'=>'admin'
        ]);
    }
}
