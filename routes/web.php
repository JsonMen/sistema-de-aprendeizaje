<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FrontController@index')->name('front.index');
Route::get('categories/{name}','FrontController@searchCategory')->name('front.search.category');
Route::get('tags/{name}','FrontController@searchTag')->name('front.search.tags');
Route::get('articles/{slug}','FrontController@viewArticle')->name('front.view.article');
Route::group(['prefix' => 'admin','middleware'=>['auth','admin']], function(){
    Route::get('/',function(){
        return view('admin.index');
    })->name('admin.index');
    Route::resource('users','UsersController')->except(['destroy']);
    Route::get('users/{id}/destroy','UsersController@destroy')->name('users.destroy');
    Route::resource('categories','CategoriesController')->except(['destroy']);
    Route::get('categories/{id}/destroy','CategoriesController@destroy')->name('categories.destroy');
    Route::resource('tags','TagsController')->except(['destroy']);
    Route::get('tags/{id}/destroy','TagsController@destroy')->name('tags.destroy');
    Route::resource('articles','ArticlesController')->except('destroy');
    Route::get('articles/{id}/destroy','ArticlesController@destroy')->name('articles.destroy');
    Route::get('images','ImagesController@index')->name('images.index');
});
/*Auth::routes();
/**/
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

/*// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');*/




Route::get('/home', 'HomeController@index')->name('home');
